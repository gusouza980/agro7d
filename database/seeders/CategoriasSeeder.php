<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categorias')->insert([
            'nome' => 'noticias',
        ]);

        DB::table('categorias')->insert([
            'nome' => 'cases',
        ]);

        DB::table('categorias')->insert([
            'nome' => 'podcast',
        ]);

        DB::table('categorias')->insert([
            'nome' => 'videos',
        ]);
    }
}
