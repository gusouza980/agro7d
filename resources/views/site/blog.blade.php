@extends('site.template.main')

@section('conteudo')
    <div class="container-fluid px-2 px-lg-5 py-5 mt-5">
        <div class="row">
            <div class="col-12 agro-section1-content text-center text-lg-left px-5">
                <h1>Mundo Agro</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid px-2 px-lg-5 py-5">
        <div class="row px-lg-5">
            <div class="col-12 nav-blog text-center text-lg-left">
                <ul class="px-0">
                    @if($noticias->count() > 0)
                        <li class="nav-blog-link @if(url()->current() == route('site.blog')) nav-blog-link-active @endif">
                            <a class="border-bottom-link-azul2" href="{{route('site.blog')}}"><span class="border-bottom-azul2">Todo</span>s</a>
                        </li>
                    @endif
                    @foreach(App\Models\Categoria::all() as $categoria)
                        @if($categoria->noticias->where("publicada", true)->count())
                            <li class="nav-blog-link @if(url()->current() == route('site.blog.categoria', ['slug' => $categoria->slug])) nav-blog-link-active @endif ml-md-4 ml-lg-5">
                                <a class="border-bottom-link-azul2" href="{{route('site.blog.categoria', ['slug' => $categoria->slug])}}">
                                    <span class="border-bottom-azul2">{{ucfirst(mb_substr($categoria->nome, 0, 4))}}</span>{{mb_substr($categoria->nome, 4, strlen($categoria->nome))}}
                                </a>
                            </li>
                        @endif
                    @endforeach
                    {{--  @if(\App\Models\Categoria::where("nome", "noticias")->first()->noticias->where("publicada", true)->count())
                        <li class="nav-blog-link @if(url()->current() == route('site.blog.categoria', ['slug' => 'noticias'])) nav-blog-link-active @endif ml-md-4 ml-lg-5">
                            <a class="border-bottom-link-azul2" href="{{route('site.blog.categoria', ['slug' => 'noticias'])}}"><span class="border-bottom-azul2">Notí</span>cias</a>
                        </li>
                    @endif
                    @if(\App\Models\Categoria::where("nome", "cases")->first()->noticias->where("publicada", true)->count())
                        <li class="nav-blog-link @if(url()->current() == route('site.blog.categoria', ['slug' => 'cases'])) nav-blog-link-active @endif ml-md-4 ml-lg-5">
                            <a class="border-bottom-link-azul2" href="{{route('site.blog.categoria', ['slug' => 'cases'])}}"><span class="border-bottom-azul2">Case</span>s</a>
                        </li>
                    @endif
                    @if(\App\Models\Categoria::where("nome", "podcast")->first()->noticias->where("publicada", true)->count())
                        <li class="nav-blog-link @if(url()->current() == route('site.blog.categoria', ['slug' => 'podcast'])) nav-blog-link-active @endif ml-md-4 ml-lg-5">
                            <a class="border-bottom-link-azul2" href="{{route('site.blog.categoria', ['slug' => 'podcast'])}}"><span class="border-bottom-azul2">Podc</span>ast</a>
                        </li>
                    @endif
                    @if(\App\Models\Categoria::where("nome", "videos")->first()->noticias->where("publicada", true)->count())
                        <li class="nav-blog-link @if(url()->current() == route('site.blog.categoria', ['slug' => 'videos'])) nav-blog-link-active @endif ml-md-4 ml-lg-5">
                            <a class="border-bottom-link-azul2" href="{{route('site.blog.categoria', ['slug' => 'videos'])}}"><span class="border-bottom-azul2">Víde</span>os</a>
                        </li>
                    @endif  --}}
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container-fluid px-5">
        @php
            $noticia = $noticias->where("destaque", true)->first();
            if(!$noticia){
                $noticia = $noticias->sortByDesc("created_at")->first();
            }
        @endphp
        @if($noticia)
            <div class="row mt-5 px-0 px-lg-5">
                <div class="col-12 col-lg-5 px-0 blog-section1-card-content">
                    <img src="{{asset($noticia->preview)}}" alt="Noticia">
                </div>
                <div class="col-12 col-xl-5 col-lg-7 py-2 px-0 px-lg-5 blog-section1-card-content text-center text-lg-left">
                    <button class="px-4 py-1 button-roxo mt-4 mt-lg-0">{{ucfirst($noticia->categoria->nome)}}</button>
                    <h2 class="mb-5 mb-lg-2 mb-xl-3 mt-3">{{$noticia->titulo}} </h2>
                    <span>{{$noticia->subtitulo}}</span>
                    <div class="blog-section1-card-content mt-4">
                        <a href="{{route('site.noticia', ['slug' => $noticia->slug])}}" class="border-bottom-link-azul2"><span class="border-bottom-azul2">Sabe</span>r mais</a>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="container-fluid py-5 px-5">
        <div class="row px-5">
            @if($noticias->count() > 0)
                @foreach($noticias as $noticia)
                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 text-center text-md-left mt-5">
                        <div class="row mt-5">
                            <div class="col-12">
                                <div class="blog-section2-card-image">
                                    <img src="{{asset($noticia->preview)}}" alt="Noticia">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 text-left">
                                <div class="blog-section2-cand-content">
                                    <h2><a href="{{route('site.blog.categoria', ['slug' => $noticia->categoria->slug])}}">{{mb_strtoupper($noticia->categoria->nome)}}</a></h2>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 text-left">
                                <div class="blog-section2-cand-content">
                                    <h3><a href="{{route('site.noticia', ['slug' => $noticia->slug])}}">{{$noticia->titulo}}</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 text-left">
                                <div class="blog-section2-cand-content">
                                    <span>{{$noticia->subtitulo}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    @include('site.includes.vamos-colher')
@endsection