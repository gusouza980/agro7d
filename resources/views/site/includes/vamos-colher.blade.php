<div class="container-fluid p-relative" id="home-section4">
  <img id="imagem-fazendeiro" src="{{asset('site/imagens/home-section4-farmer.png')}}" alt="Fazendeiro">
  <div class="row mt-5 justify-content-center">
      <div class="col-12 col-md-11 col-lg-9 col-xl-7 text-center home-section4-title">
          <h1>Queremos transformar seus objetivos em realidade.</h1>
          <h2>Vamos juntos colher os melhores frutos?</h2>
      </div>
  </div>
  <div class="row mt-4 justify-content-center">
      <div class="col-12 col-md-10 col-lg-9 col-xl-6 home-section4-content text-center">
          <span>A Agro7D entrega as melhores e mais completas soluções de marketing digital, gerando maior visibilidade, autoridade para sua marca e novas oportunidades para o seu negócio.</span>
      </div>
  </div>
  <div class="row mt-5 justify-content-center">
      <div class="col-12 text-center">
          <a href="https://api.whatsapp.com/send?phone=553588559461" target="_blank"><button class="btn-verde-agende px-4 py-3">AGENDE UMA LIGAÇÃO ESTRATÉGICA</button></a>
      </div>
  </div>
</div>