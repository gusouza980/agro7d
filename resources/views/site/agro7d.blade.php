@extends('site.template.main')

@section('conteudo')
    <div class="container-fluid px-2 px-lg-5 pt-5">
        <div class="row">
            <div class="col-12 agro-section1-content text-center text-lg-left px-5">
                <h1>Agro7d</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-lg-7 px-5 agro-section1-content text-center text-lg-left">
                <p>Aprendemos com nossas raízes a importância da comunicação. Cultivamos no coração da nossa equipe o entusiasmo e paixão pela criatividade. Colhemos o fruto da inovação vinculada aos canais digitais. Assim nasceu a Agro7D.</p>
                <p>Somos a ponte entre o campo e o mundo digital. Desenvolvemos soluções em marketing digital para romper as fronteiras da comunicação e criar conexões.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="home-section3-diagonal">
        <div class="row">
            <div class="col-12">

            </div>
        </div>
    </div>
    <div class="container-fluid bg-cinza1 py-5 px-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 col-xl-4 text-center agro-section2-title">
                <h1>Resultados acima de 7 dígitos</h1>
            </div>
        </div>
        <div class="row justify-content-center mt-4">
            <div class="col-12 col-lg-6 text-center agro-section2-title">
                <h2>Ajudamos empresas do agronegócio a serem vistas e reconhecidas, criar um relacionamento duradouro com o público e gerar resultados acima de 7 Dígitos.</h2>
            </div>
        </div>
        <div class="row justify-content-center mt-5 px-md-5">
            <div class="col-12 col-md-6 col-lg-3 mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section2-icon bg-azul2 mx-auto">
                            <i class="fab fa-hubspot text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12 text-center agro-section2-card-content">
                        <h3>Ponta a Ponta</h3>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-12 col-xl-8 text-center agro-section2-card-content">
                        <span>Serviços de ponta a ponta para a cadeia, com olhar diferenciado para o antes, dentro e após a porteira.</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section2-icon bg-azul2 mx-auto">
                            <img src="{{asset('site/imagens/agro-section2-icone-360.png')}}" alt="360">
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12 text-center agro-section2-card-content">
                        <h3>Agência 360</h3>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-12 col-xl-8 text-center agro-section2-card-content">
                        <span>Da estratégia de marca ao marketing de mídia social, do design gráfico ao desenvolvimento web.</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section2-icon bg-azul2 mx-auto">
                            <i class="fas fa-expand-arrows-alt text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12 text-center agro-section2-card-content">
                        <h3>Full Stack</h3>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-12 col-xl-8 text-center agro-section2-card-content">
                        <span>Temos as melhores soluções para gerar maior exposição da empresa, interações bem-sucedidas com o cliente, melhor envolvimento do usuário e novas oportunidades de negócios.</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section2-icon bg-azul2 mx-auto">
                            <i class="fas fa-bezier-curve text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12 text-center agro-section2-card-content">
                        <h3>Criatividade</h3>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-12 col-xl-8 text-center agro-section2-card-content">
                        <span>A equipe criativa certa pode apresentar sua mensagem ao público certo e ajudá-lo a ter um relacionamento com sucesso. Isso é o que fazemos todos os dias.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 pb-5">
            <div class="col-12 text-center mt-5">
                <a href="{{route('site.solucoes')}}"><button class="btn-verde-agende px-4 py-3"><i class="fas fa-chevron-right"></i> NOSSAS SOLUÇÕES</button></a>
            </div>
        </div>
    </div>
    <div class="container-fluid py-5 px-5">
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-lg-6 col-xl-4 text-center agro-section3-title">
                <h1>Nossos pilares</h1>
            </div>
        </div>
        <div class="row justify-content-center mt-4">
            <div class="col-12 col-lg-6 text-center agro-section3-title">
                <h2>Seguimos uma missão simples, onde acreditamos que uma estratégia bem definida e eficiente é o caminho inicial para fertilizar o sucesso do seu negócio. Desenvolvemos a metodologia 7D para te ajudar nessa jornada.</h2>
            </div>
        </div>
        <div class="row justify-content-center mt-5 px-md-5">
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="fas fa-code text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Tecnologia</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="far fa-lightbulb text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Mindset Disruptivo</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="fas fa-chess-knight text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Estratégia</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="fas fa-globe-americas text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Inovação</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row mt-0 mt-lg-5">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <img src="{{asset('site/imagens/agro-section3-icon-idea.png')}}" alt="Ideia">
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Empreendedorismo</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row mt-0 mt-lg-5">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="fas fa-medal text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>ESG</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mt-5 py-5 py-lg-0">
                <div class="row mt-0 mt-lg-5">
                    <div class="col-12">
                        <div class="agro-section3-icon bg-azul2 mx-auto">
                            <i class="fas fa-heart text-white"></i>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-center agro-section3-card-content">
                        <h3>Relacionamento</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('site.includes.vamos-colher')
@endsection