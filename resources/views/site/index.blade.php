@extends('site.template.main')
@section('conteudo')
    <div class="container-fluid d-none d-md-block" class="">
        <div class="row">
            <div class="col-12 px-0">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('site/imagens/banners/banner1.jpg') }}"
                                alt="First slide">       
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('site/imagens/banners/banner2.jpg') }}"
                                alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('site/imagens/banners/banner3.jpg') }}"
                                alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> 
                <div class="home-carousel-content">
                    <h1>Nós pensamos o<br>Agro diferente!</h1>
                    <h2>Marketing Digital, Inteligência,<br>Tecnologia e Resultados</h2>
                </div>
                <div class="home-carousel-redes">
                    <a href="https://www.facebook.com/Agro7D" target="_blank"><i class="fab fa-facebook text-white"></i></a>
                    <a href="https://www.instagram.com/agro7d/" target="_blank"><i class="fab fa-instagram text-white mx-3"></i></a>
                    <a href="https://www.linkedin.com/company/agro7d" target="_blank"><i class="fab fa-linkedin text-white"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid d-block d-md-none py-5" style="background: url({{asset('site/imagens/banners/banner1.jpg')}}); background-repeat: no-repeat; background-size: cover; object-fit: cover;" class="">
        <div class="row py-5">
            <div class="col-12 px-0">
                <div class="home-carousel-content-xs text-center">
                    <h1>Nós pensamos o<br>Agro diferente!</h1>
                    <h2>Marketing Digital, Inteligência,<br>Tecnologia e Resultados</h2>
                </div>
                <div class="home-carousel-redes-xs text-center mt-4">
                    <a href="https://www.facebook.com/Agro7D" target="_blank"><i class="fab fa-facebook text-white fa-lg"></i></a>
                    <a href="https://www.instagram.com/agro7d/" target="_blank"><i class="fab fa-instagram text-white mx-3 fa-lg"></i></a>
                    <a href="https://www.linkedin.com/company/agro7d" target="_blank"><i class="fab fa-linkedin text-white fa-lg"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row mt-5">
            <div class="col-12 text-center home-section2-title">
                <h1>Chegou a semente que faltava</h1>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 text-center home-section2-title">
                <h2>Cultivamos soluções em Marketing Digital para colher os melhores resultados. Somos especializados em toda cadeia de comunicação do agronegócio, aplicamos conhecimento estratégico em canais digitais, levando inovação e tecnologia para o campo.</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 d-flex justify-content-center text-center home-section2-title link-hover">
                <a href="{{route('site.agro')}}">
                    <div class="home-section2-title p-relative link-hover" style="width: 150px;">
                        <img class="icone-folha-section2-hover" style="display:none;" src="{{asset('site/imagens/icone-folha.png')}}">
                        <h5>
                            A Agro7d
                        </h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-5 px-5 p-relative">
        <div id="home-section2-folha">
            <img src="{{asset('site/imagens/folha.png')}}" alt="Folha">
        </div>
        <div id="home-section2-cafe1">
            <img src="{{asset('site/imagens/cafe1.png')}}" alt="Folha">
        </div>
        <div id="home-section2-cafe2">
            <img src="{{asset('site/imagens/cafe2.png')}}" alt="Folha">
        </div>
        <div class="row px-lg-5 px-2">
            <div class="col-12 col-lg-6 home-section2-content text-center">
                <img src="{{asset('site/imagens/home-section2-farmer.png')}}" alt="Fazendeiro">
            </div>
            <div class="col-12 col-lg-6 home-section2-content mt-5 text-center text-lg-left">
                <div class="row d-none d-lg-flex">
                    <div class="col-12 home-section2-content">
                        <h1>Queremos<br>ouvir sua<br>história</h1>
                    </div>
                </div>
                <div class="row d-flex d-lg-none">
                    <div class="col-12 home-section2-content">
                        <h1>Queremos ouvir sua<br>história</h1>
                    </div>
                </div>
                <div class="row mt-5 mt-lg-3">
                    <div class="col-12 home-section2-content pr-lg-5">
                        <span>Para trabalhar estratégias eficientes e preencher a lacuna entre onde você está agora e onde desejar chegar. Promovemos a sua marca, com olhar diferenciado para o antes, dentro e após a porteira, criando interações bem-sucedidas e levando sua história até seu público.</span>
                    </div>
                </div>
                <div class="row mt-5 mt-lg-4">
                    <div class="col-12 home-section2-content">
                        <a href="{{route('site.contato')}}"><button class="btn-verde px-4">QUAL SEU MAIOR DESAFIO ?</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="home-section3-diagonal">
        <div class="row">
            <div class="col-12">

            </div>
        </div>
    </div>
    <div class="container-fluid bg-cinza1" id="home-section3">
        <div class="row">
            <div class="col-12 text-center home-section3-title mt-5">
                <h1>Soluções</h1>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-12 col-lg-5 text-center home-section3-title">
                <h2>A Agro7D se dedica a tornar sua presença online atraente para seu público-alvo e a ser orientado na busca de resultados acima de 7 Dígitos para o seu negócio.</h2>
            </div>
        </div>
    </div>
    

    
    <div class="container-fluid py-5 px-5" id="home-section3-cards">
        <div id="container-section-cards">
            <div class="row justify-content-center justify-content-lg-start px-2 px-md-5">
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Inbound Marketing</h1>
                                    <span>Desenvolvemos estratégias de marketing que visam atrair e converter clientes usando conteúdo relevante.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Outbound Marketing</h1>
                                    <span>Criamos campanhas ativas para atrair o público ideal para o seu negócio.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>SEO</h1>
                                    <span>Criamos campanhas ativas para atrair o público ideal para o seu negócio.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Business Intelligence</h1>
                                    <span>Captamos dados para analisar informações e oferecer suporte a gestão de negócios.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Mídia de Performance</h1>
                                    <span>Desenvolvemos, segmentamos e medimos campanhas de anúncios para os melhores resultados.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Tecnologia da Informação</h1>
                                    <span>Desenvolvemos soluções tecnológicas para alavancar os seus resultados.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                    <div class="home-section3-card box-shadow-1 mx-auto">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 px-0">
                                    <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-4 pb-3">
                            <div class="row">
                                <div class="col-12 px-3 home-section3-card-content text-center">
                                    <h1>Gerenciamento Mídias Sociais</h1>
                                    <span>Gestão estratégica dos canais digitais para posicionamento de marca, com os objetivos de atrair, reter e engajar o público.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('site.includes.vamos-colher')
@endsection