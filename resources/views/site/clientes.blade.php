@extends('site.template.main')

@section('conteudo')
    <div class="container-fluid px-2 px-lg-5 py-5 mt-5" id="clientes-section1">
        <div class="row">
            <div class="col-12 agro-section1-content text-center text-lg-left px-5">
                <h1>Clientes</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-lg-7 px-5 agro-section1-content text-center text-lg-left">
                <p>Veja alguns dos nossos clientes que já estão colhendo<br>resultados com a metodologia 7D:</p>
            </div>
        </div>
    </div>
    {{--  <div class="container-fluid" id="home-section3-diagonal">
        <div class="row">
            <div class="col-12">
                
            </div>
        </div>
    </div>  --}}
    <div class="container-fluid py-5" id="clientes-section2">
        <div class="container">
            <div class="row pb-5 align-items-center row-cliente-card">
                <div class="col-12 col-lg-4 div-cliente-card">
                    <div class="cliente-card box-shadow-1 d-flex justify-content-center align-items-center mx-auto">
                        <img class="" width="200" height="160" src="{{asset('site/imagens/logo-6grain.png')}}" alt="6th Grain">
                    </div>
                </div>
                <div class="col-12 col-lg-4 div-cliente-card">
                    <div class="mt-5 mt-lg-0 cliente-card box-shadow-1 d-flex justify-content-center align-items-center mx-auto">
                        <img class="" width="300" height="100" src="{{asset('site/imagens/logo-inova.png')}}" alt="Inova">
                    </div>
                </div>
                <div class="col-12 col-lg-4 div-cliente-card">
                    <div class="mt-5 mt-lg-0 cliente-card box-shadow-1 d-flex justify-content-center align-items-center mx-auto">
                        <img class="" width="244" height="200" src="{{asset('site/imagens/logo-ipanema.png')}}" alt="Ipanema">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('site.includes.vamos-colher')
@endsection