@extends('site.template.main')

@section('conteudo')
    <div class="container-fluid px-2 px-lg-5 py-5 mt-5">
        <div class="row">
            <div class="col-12 col-lg-10 col-xl-9 agro-section1-content text-center text-lg-left px-5">
                <h1>Queremos ouvir a sua história!</h1>
            </div>
            <div class=" mt-5 d-none d-lg-block col-lg-2 col-xl-3 text-left">
                <a href="https://www.facebook.com/Agro7D" target="_blank"><i
                        class="fab fa-facebook fa-2x text-azul2"></i></a>
                <a href="https://www.instagram.com/agro7d/" target="_blank"><i
                        class="fab fa-instagram fa-2x text-azul2 mx-2"></i></a>
                <a href="https://www.linkedin.com/company/agro7d" target="_blank"><i
                        class="fab fa-linkedin fa-2x text-azul2"></i></a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12 col-lg-7 col-xl-6 px-5 agro-section1-content text-center text-lg-left">
                <p>Conte para a gente onde quer chegar.<br>Estamos prontos para te ajudar a potencializar sua presença
                    digital e atingir resultados surpreendentes.</p>
            </div>
        </div>
        <div class="row mt-4 d-flex d-lg-none">
            <div class="col-12 text-center">
                <a href="https://www.facebook.com/Agro7D" target="_blank"><i
                        class="fab fa-facebook fa-2x text-azul2"></i></a>
                <a href="https://www.instagram.com/agro7d/" target="_blank"><i
                        class="fab fa-instagram fa-2x text-azul2 mx-2"></i></a>
                <a href="https://www.linkedin.com/company/agro7d" target="_blank"><i
                        class="fab fa-linkedin fa-2x text-azul2"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="contato-section2-diagonal">
        <div class="row">

        </div>
    </div>
    <div class="container-fluid bg-cinza1 py-5">
        <div class="container contato-container py-5">
            <div class="row py-5">
                <div class="col-12 text-center text-lg-left contato-form-content">
                    <h3>ENVIE UMA MENSAGEM</h3>
                </div>
            </div>
            <form id="form-contato" action="{{ route('site.email') }}" method="post">
                @csrf
                <div class="row">
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">Digite seu nome</label>
                        <input type="text" class="form-control py-4 mt-3 ml-n2" name="nome" id="" aria-describedby="helpId"
                            placeholder="Digite seu nome" required>
                    </div>
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">Digite seu email</label>
                        <input type="email" class="form-control py-4 mt-3 ml-n2" name="email" id=""
                            aria-describedby="helpId" placeholder="Digite seu email" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">Qual é sua empresa?</label>
                        <input type="text" class="form-control py-4 mt-3 ml-n2" name="empresa" id=""
                            aria-describedby="helpId" placeholder="Digite o nome fantasia" required>
                    </div>
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">Qual é seu cargo?</label>
                        <input type="text" class="form-control py-4 mt-3 ml-n2" name="cargo" id="" aria-describedby="helpId"
                            placeholder="Digite seu cargo" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">Telefone</label>
                        <input type="text" class="form-control py-4 mt-3 ml-n2" name="telefone" id=""
                            aria-describedby="helpId" placeholder="Digite seu telefone" required>
                    </div>
                    <div class="form-group col-12 col-lg-6 contato-form-content">
                        <label for="">WhatsApp (Opcional)</label>
                        <input type="text" class="form-control py-4 mt-3 ml-n2" name="whatsapp" id=""
                            aria-describedby="helpId" placeholder="Digite seu WhatsApp">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="form-group col-12 contato-form-content">
                        <label for="">Principal Assunto</label>
                        <select class="form-control ml-n2" name="assunto" placeholder="Selecione o assunto">
                            <option value="">Selecione o assunto</option>
                            <option value="Quero conversar com o Comercial Agro7D">Quero conversar com o Comercial Agro7D
                            </option>
                            <option value="Quero contar minha história">Quero contar minha história</option>
                            <option value="Quero conversar com estrategista Agro7D">Quero conversar com estrategista Agro7D
                            </option>
                            <option value="Falar no RH">Falar no RH</option>
                            <option value="Falar no Administrativo Agro7D">Falar no Administrativo Agro7D</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="form-group col-12 contato-form-content">
                        <label for="">Digite sua mensagem</label>
                        <textarea class="form-control mt-3 py-4 px-4" name="mensagem" id="" rows="10"
                            placeholder="Digite sua mensagem aqui" required></textarea>
                    </div>
                </div>
                <div class="row mt-3" id="contato-botao-enviar">
                    <div class="col-12 text-center text-lg-left contato-form-content">
                        <button type="submit" class="botao-submit-contato" style=""><a
                                class="border-bottom-link-azul2 c-pointer"><span
                                    class="border-bottom-azul2">Envi</span>ar</a></button>
                    </div>
                </div>
                <div class="row d-none" id="contato-ajax-loading">
                    <div class="col-12 text-center">
                        <img src="{{asset('site/imagens/ajax-loading.gif')}}" width="80" alt="">
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('site.includes.vamos-colher')
    <!-- Modal -->
    <div class="modal fade" id="modalContatoSucesso" tabindex="-1" role="dialog" aria-labelledby="modalContatoSucessoLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h5>Obrigado! Recebemos sua mensagem e entraremos em contato assim que possível.</h5>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                                Certo!
                            </button>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalContatoErro" tabindex="-1" role="dialog" aria-labelledby="modalContatoErroLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h5>Erro ao enviar a mensagem. Tente novamente mais tarde.</h5>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                                Certo!
                            </button>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
