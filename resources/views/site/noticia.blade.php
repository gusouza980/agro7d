@extends('site.template.main')

@section("metas")
    <meta name="keywords" content="{{implode(',', $tags)}}"/>
@endsection

@section('conteudo')

    @if($noticia->banner)
        <div class="container py-5">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="{{asset($noticia->banner)}}" class="w-100" alt="Banner">
                </div>
            </div>
        </div>
    @endif
    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{route('site.blog')}}" class="link-voltar-blog">Voltar para o Mundo Agro</a>
            </div>
        </div>
    </div>

    <div class="container px-5">
        <div class="row">
            <div class="col-12 text-center noticia-content">
                <h1>{!! $noticia->titulo !!}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center noticia-content">
                <h3>{!! $noticia->subtitulo !!}</h3>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 noticia-content">
                {!! $noticia->conteudo !!}
            </div>
        </div>
    </div>

    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{route('site.blog')}}" class="link-voltar-blog">Voltar para o Mundo Agro</a>
            </div>
        </div>
    </div>
    
    @include('site.includes.vamos-colher')
@endsection
