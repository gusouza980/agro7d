<!doctype html>
<html lang="en">

<head>
    <title>Agro7d</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Agro7D – Somos a semente que faltava! Somos uma equipe experiente com conhecimento estratégico em canais digitais e especializados na cadeia do agronegócio. Plantamos o marketing aliado a ciência de dados, para colher os melhores resultados para o seu negócio." name="description" />
    <meta content="Luis Gustavo de Souza Carvalho" name="author" />
    <meta name="_token" content="{{ csrf_token() }}">
    @yield('metas')

    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">  

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('site/css/main.css') }}">

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />

</head>

<body>
    <div id="nav-height">
        <div class="container-fluid text-left py-0 py-lg-4 d-block d-lg-flex align-items-center" id="container-nav">
            <div class="nav-container w-100">
                <div class="row align-items-center d-lg-flex d-none">
                    <div class="col-2 text-center">
                        <a href="{{route('site.index')}}"><img class="" style="height: 60px;" src="{{asset('site/imagens/logo.png')}}" alt="Logo Agro7d"></a>
                    </div>
                    <div class="col-7 text-center navbar-lg-content">
                        <ul class="text-center mt-3">
                            <li class="mr-3 p-relative">
                                <a @if(url()->current() == route('site.agro')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.agro')}}">
                                    @if(url()->current() == route('site.agro'))
                                        <img src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-1">
                                    @else
                                        <img class="icone-folha-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    Agro7d
                                </a>
                            </li>
                            <li class="mx-3 p-relative">
                                <a @if(url()->current() == route('site.solucoes')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.solucoes')}}">
                                    @if(url()->current() == route('site.solucoes'))
                                        <img src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-1">
                                    @else
                                        <img class="icone-folha-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    Soluções
                                </a>
                            </li>
                            <li class="mx-3 p-relative">
                                <a @if(url()->current() == route('site.clientes')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.clientes')}}">
                                    @if(url()->current() == route('site.clientes'))
                                        <img src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-1">
                                    @else
                                        <img class="icone-folha-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    Clientes
                                </a>
                            </li>
                            <li class="mx-3 p-relative">
                                <a @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos'])) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.blog')}}">
                                    @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos']))
                                        <img src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-1">
                                    @else
                                        <img class="icone-folha-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    Mundo Agro
                                </a>
                            </li>
                            <li class="ml-3 p-relative">
                                <a @if(url()->current() == route('site.contato')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.contato')}}">
                                    @if(url()->current() == route('site.contato'))
                                        <img src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-1">
                                    @else
                                        <img class="icone-folha-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    Contato
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-3 text-center d-lg-flex align-items-center d-none">
                        <a href="{{route('site.contato')}}"><button class="btn-verde px-4 py-2"><i class="fas fa-chevron-right"></i> VAMOS CONVERSAR?</button></a>
                    </div>
                </div>
            </div>
            <nav class="navbar w-100 navbar-expand-lg navbar-light d-block d-lg-none" style="min-height: 80px;">
                <a class="navbar-brand" href="{{route('site.index')}}"><img class="" style="height: 50px;" src="{{asset('site/imagens/logo.png')}}" alt="Logo Agro7d"></a>
                <button class="navbar-toggler float-right mt-3" style="color:white;" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon" style="color:white;"><i class="fas fa-bars"></i></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto bg-branco text-center">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('site.agro')}}">
                                <span class="p-relative">   
                                    @if(url()->current() == route('site.agro'))
                                        <img style="position: absolute; left: -20px; top: 8px;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    <span class="span-link @if(url()->current() == route('site.agro')) link-active @endif">Agro7d</span>
                                </span> 
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('site.solucoes')}}">
                                <span class="p-relative">   
                                    @if(url()->current() == route('site.solucoes'))
                                        <img style="position: absolute; left: -20px; top: 8px;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    <span class="span-link @if(url()->current() == route('site.solucoes')) link-active @endif">Soluções</span>
                                </span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('site.clientes')}}">
                                <span class="p-relative">   
                                    @if(url()->current() == route('site.clientes'))
                                        <img style="position: absolute; left: -20px; top: 8px;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    <span class="span-link @if(url()->current() == route('site.clientes')) link-active @endif">Clientes</span>
                                </span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('site.blog')}}">
                                <span class="p-relative">   
                                    @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos']))
                                        <img style="position: absolute; left: -20px; top: 8px;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    <span class="span-link @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos'])) link-active @endif">Mundo Agro</span>
                                </span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('site.contato')}}">
                                <span class="p-relative">   
                                    @if(url()->current() == route('site.contato'))
                                        <img style="position: absolute; left: -20px; top: 8px;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                                    @endif
                                    <span class="span-link @if(url()->current() == route('site.contato')) link-active @endif">Contato</span>
                                </span> 
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="espaco-nav"></div>
    @yield("conteudo")
    <div class="container-fluid py-5 px-lg-5 bg-azul1" style="z-index: 9999;">
        <div class="row align-items-center">
            <div class="col-12 col-lg-4 text-center">
                <img style="max-width: 200px;" src="{{asset('site/imagens/logo.png')}}" alt="Logo Agro7D">
            </div>
            <div class="col-12 col-lg-2 text-left text-white footer-content text-center text-lg-left">
                <ul class="my-5 my-lg-0 px-0" style="width: 120px;">
                    <li class="p-relative">
                        <a @if(url()->current() == route('site.agro')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.agro')}}">
                            @if(url()->current() == route('site.agro'))
                                <img class="icone-folha-footer-hover" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @else
                                <img class="icone-folha-footer-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @endif
                            Agro7d
                        </a>
                    </li>
                    <li class="p-relative">
                        <a @if(url()->current() == route('site.solucoes')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.solucoes')}}">
                            @if(url()->current() == route('site.solucoes'))
                                <img class="icone-folha-footer-hover" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @else
                                <img class="icone-folha-footer-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @endif
                            Soluções
                        </a>
                    </li>
                    <li class="p-relative">
                        <a @if(url()->current() == route('site.clientes')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.clientes')}}">
                            @if(url()->current() == route('site.clientes'))
                                <img class="icone-folha-footer-hover" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @else
                                <img class="icone-folha-footer-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @endif
                            Clientes
                        </a>
                    </li>
                    <li class="p-relative">
                        <a @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos'])) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.blog')}}">
                            @if(url()->current() == route('site.blog') || url()->current() == route('site.blog.categoria', ['slug' => 'noticias']) || url()->current() == route('site.blog.categoria', ['slug' => 'cases']) || url()->current() == route('site.blog.categoria', ['slug' => 'podcast']) || url()->current() == route('site.blog.categoria', ['slug' => 'videos']))
                                <img class="icone-folha-footer-hover" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @else
                                <img class="icone-folha-footer-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @endif
                            Mundo Agro
                        </a>
                    </li>
                    <li class="p-relative">
                        <a @if(url()->current() == route('site.contato')) class="link-active-azul" @else class="link-hover" @endif href="{{route('site.contato')}}">
                            @if(url()->current() == route('site.contato'))
                                <img class="icone-folha-footer-hover" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @else
                                <img class="icone-folha-footer-hover" style="display: none;" src="{{asset('site/imagens/icone-folha.png')}}" width="10" class="mr-2">
                            @endif
                            Contato
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-lg-2 text-center text-white my-5 my-lg-0">
                <a href="https://www.facebook.com/Agro7D" target="_blank"><i class="fab fa-facebook fa-2x text-white"></i></a>
                <a href="https://www.instagram.com/agro7d/" target="_blank"><i class="fab fa-instagram fa-2x text-white mx-2"></i></a>
                <a href="https://www.linkedin.com/company/agro7d" target="_blank"><i class="fab fa-linkedin fa-2x text-white"></i></a>
            </div>
            <div class="col-12 col-lg-4 text-center">
                <a href="{{route('site.contato')}}"><button class="btn-verde px-4 py-2"><i class="fas fa-chevron-right"></i> VAMOS CONVERSAR?</button></a>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script>

        $(document).ready(function(){
            $(".link-hover").mouseover(function(){
                $(this).children("img").show();
                $(this).addClass("link-active-azul");
                $(this).children("h5").addClass("link-active-azul");
            })

            $(".link-hover").mouseout(function(){
                $(this).children("img").hide();
                $(this).removeClass("link-active-azul");
                $(this).children("h5").removeClass("link-active-azul");
            })

            $("#form-contato").submit(function(e){
                e.preventDefault();
                var nome = $("input[name='nome']").val();
                var email = $("input[name='email']").val();
                var empresa = $("input[name='empresa']").val();
                var cargo = $("input[name='cargo']").val();
                var telefone = $("input[name='telefone']").val();
                var whatsapp = $("input[name='whatsapp']").val();
                var assunto = $("select[name='assunto']").val();
                var mensagem = $("textarea[name='mensagem']").val();
                var _token = $('meta[name="_token"]').attr('content');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': _token
                    }
                });  

                $.ajax({
                    url: '/contato/enviar',
                    type: 'POST',
                    data: {
                        nome: nome,
                        email: email,
                        empresa: empresa,
                        cargo: cargo,
                        telefone: telefone,
                        whatsapp: whatsapp,
                        assunto: assunto,
                        mensagem: mensagem
                    },
                    dataType: 'JSON',
                    beforeSend: function(){
                        $("#contato-botao-enviar").addClass("d-none");
                        $("#contato-ajax-loading").removeClass("d-none");
                    },
                    success: function(data) {
                        if(data = "sucesso"){
                            $("#modalContatoSucesso").modal();
                        }else{
                            $("#modalContatoErro").modal();
                        }
                        $("#contato-botao-enviar").removeClass("d-none");
                        $("#contato-ajax-loading").addClass("d-none");
                    },
                    error: function(err){
                        $("#modalContatoErro").modal();
                        $("#contato-botao-enviar").removeClass("d-none");
                        $("#contato-ajax-loading").addClass("d-none");
                    }
                });
            });
        })
    </script>
</body>

</html>
