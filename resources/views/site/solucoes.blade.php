@extends('site.template.main')

@section('conteudo')
    <div class="container-fluid px-2 px-lg-5 py-5 mt-5">
        <div class="row">
            <div class="col-12 agro-section1-content text-center text-lg-left px-5">
                <h1>Soluções</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-lg-7 px-5 agro-section1-content text-center text-lg-left">
                <p>A Agro7D entrega as melhores e mais completas soluções de marketing digital para transformar seus objetivos em realidade. Possibilitamos oportunidades de crescimento utilizando tecnologias modernas e trazendo inovação para o campo.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid py-5 px-5">
        <div class="row justify-content-center justify-content-lg-start px-2 px-md-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Inbound Marketing</h1>
                                <span>Desenvolvemos estratégias de marketing que visam atrair e converter clientes usando conteúdo relevante.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Outbound Marketing</h1>
                                <span>Criamos campanhas ativas para atrair o público ideal para o seu negócio.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>SEO</h1>
                                <span>Criamos campanhas ativas para atrair o público ideal para o seu negócio.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Business Intelligence</h1>
                                <span>Captamos dados para analisar informações e oferecer suporte a gestão de negócios.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Mídia de Performance</h1>
                                <span>Desenvolvemos, segmentamos e medimos campanhas de anúncios para os melhores resultados.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Tecnologia da Informação</h1>
                                <span>Desenvolvemos soluções tecnológicas para alavancar os seus resultados.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 col-xl-3 px-3 mt-5">
                <div class="home-section3-card box-shadow-1 mx-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 px-0">
                                <img class="w-100" src="{{asset('site/imagens/home-section3-thumb1.png')}}" alt="Inbound Marketing">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-4 pb-3">
                        <div class="row">
                            <div class="col-12 px-3 home-section3-card-content text-center">
                                <h1>Gerenciamento Mídias Sociais</h1>
                                <span>Gestão estratégica dos canais digitais para posicionamento de marca, com os objetivos de atrair, reter e engajar o público.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('site.includes.vamos-colher')
@endsection