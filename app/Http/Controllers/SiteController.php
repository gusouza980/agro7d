<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\Categoria;
use App\Models\Visitas;
use Illuminate\Support\Str;
use App\Classes\Email;

class SiteController extends Controller
{
    //
    public function index(){
        // $file = "teste";
        // Email::enviar($file, "Contato", "fernando@agro7d.com.br", true);
        // die("saiu");
        return view("site.index");
    }

    public function agro(){
        return view("site.agro7d");
    }

    public function solucoes(){
        return view("site.solucoes");
    }

    public function clientes(){
        return view("site.clientes");
    }

    public function contato(){
        return view("site.contato");
    }

    public function blog(){
        $noticias = Noticia::where("publicada", true)->get();
        return view("site.blog", ["noticias" => $noticias]);
    }

    public function blog_categoria($slug){
        $categoria = Categoria::where("slug", $slug)->first();
        $noticias = $categoria->noticias->where("publicada", true);
        return view("site.blog", ["noticias" => $noticias]);
    }

    public function noticia($slug){
        $noticia = Noticia::where("slug", $slug)->first();
        $tags = array();
        foreach($noticia->tags as $tag){
            $tags[] = Str::slug($tag->nome);
        }

        $noticia->visualizacoes += 1;
        $noticia->save();
        
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    
        $estado = null;
        $cidade = null;
        $cep = null;
    
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
    
        if($query && $query["status"] == "success"){
            $estado = $query["region"];
            $cidade = $query["city"];
            $cep = $query["zip"];
        }

        $visita = new Visitas;
        $visita->noticia_id = $noticia->id;
        $visita->ip = $ip;
        $visita->estado = $estado;
        $visita->cidade = $cidade;
        $visita->cep = $cep;

        $visita->save();

        return view("site.noticia", ["noticia" => $noticia, "tags" => $tags]);
    }

    public function email(Request $request){
        $msg = "<b>Nome</b>: " . $request->nome . "<br>";
        $msg .= "<b>Email</b>: " . $request->email . "<br>";
        $msg .= "<b>Empresa</b>: " . $request->empresa . "<br>";
        $msg .= "<b>Cargo</b>: " . $request->cargo . "<br>";
        $msg .= "<b>Telefone</b>: " . $request->telefone . "<br>";
        $msg .= "<b>Whatsapp</b>: " . $request->whatsapp . "<br>";
        $msg .= "<b>Assunto</b>: " . $request->assunto . "<br><br><br>";
        $msg .= "<b>Mensagem</b>: <br><br>" . $request->mensagem;
        $res = Email::enviar($msg, "Contato", "info@agro7d.com.br", true);
        if($res){
            return response()->json("sucesso", 200);
        }else{
            return response()->json("erro", 200);
        }
    }

}
