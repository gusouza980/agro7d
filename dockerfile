FROM php:alpine

WORKDIR /app

ADD . /app
ADD custom.ini /usr/local/etc/php/conf.d
ARG PORT
ENV PORT $PORT
EXPOSE $PORT

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install
RUN php artisan key:generate
RUN php artisan migrate --force
# RUN php artisan db:seed --class CategoriasSeeder --force
# RUN php artisan db:seed --class UsuarioSeeder --force

RUN chmod 755 -R ./

CMD php artisan serve --host=0.0.0.0 --port=$PORT
